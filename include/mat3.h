#pragma once

#include "vec2.h"
#include "vec3.h"

namespace Math2D {

template<class T>
struct __attribute__ ((__packed__)) Mat3x3 {
//struct Vec2 {
    
    //T x, y;

    union {
    struct {
        T a11, a12, a13, a21, a22, a23, a31, a32, a33;
    };
    T v[9];
    };

    Mat3x3<T>(T a11, T a12, T a13, T a21, T a22, T a23, T a31, T a32, T a33)
    {
        this->a11 = a11;
        this->a12 = a12;
        this->a13 = a13;
        this->a21 = a21;
        this->a22 = a22;
        this->a23 = a23;
        this->a31 = a31;
        this->a32 = a32;
        this->a33 = a33;
    }

    inline Vec3<T> row1() const { return Vec3<T>(a11, a12, a13); }
    inline Vec3<T> row2() const { return Vec3<T>(a21, a22, a23); }
    inline Vec3<T> row3() const { return Vec3<T>(a31, a32, a33); }
    inline Vec3<T> col1() const { return Vec3<T>(a11, a21, a31); }
    inline Vec3<T> col2() const { return Vec3<T>(a12, a22, a32); }
    inline Vec3<T> col3() const { return Vec3<T>(a13, a23, a33); }


    static Mat3x3<T> identity()
    {
        return Mat3x3<T>(1, 0, 0, 0, 1, 0, 0, 0, 1);
    }

    static Mat3x3<T> translation(const Vec2<T> &v)
    {
        return Mat3x3<T>(1, 0, v.x, 0, 1, v.y, 0, 0, 1);
    }

    /* Clockwise rotation matrix */
    static Mat3x3<T> rotation(float rad)
    {
        return Mat3x3<T>(std::cos(rad), -std::sin(rad), 0, std::sin(rad), std::cos(rad), 0, 0, 0, 1);
    }

    /*static Mat3x3<T> rotation(float, rad, const Vec2<T> &scaling, const Vec2<T> &translation)
    {
        Mat3x3<T> mat(scaling.x*std::cos(rad), -scaling.y*std::sin(rad), translation.x*scaling.x*std::cos(rad)-translation.y*scaling.y*std::sin(rad))
    }*/

    /* Clockwise rotation about arbitrary point c  */
    static Mat3x3<T> rotation(float rad, const Vec2<T> &c)
    {
        
        auto nt = Mat3x3<T>::translation(-c);
        auto r = Mat3x3<T>::rotation(rad);
        auto t = Mat3x3<T>::translation(c);
        //return nt*r*t;
        return t*r*nt;
        /*T x = c.x;
        T y = c.y;
        return Mat3x3<T>(std::cos(rad), std::sin(rad), -x * std::cos(rad) + y * std::sin(rad) + x,
                        -std::sin(rad), std::cos(rad), -x * std::sin(rad) - y * std::cos(rad) + y, 
                        0, 0, 1);*/
    }

    
    static Mat3x3<T> scaling(const Vec2<T> &v)
    {
        return Mat3x3<T>(v.x, 0, 0, 0, v.y, 0, 0, 0, 1);
    }
    Mat3x3<T>& scale(const Vec2<T> &v)
    {
        *this = Mat3x3<T>::scaling(v)*(*this);
        return *this;
    }   

    Mat3x3<T>& rotate(float rad)
    {
        *this = Mat3x3<T>::rotation(rad)*(*this);
        return *this;
    }

    Mat3x3<T>& rotate(float rad, const Vec2<T> center)
    {
        *this = this->translate(-center);
        *this = this->rotate(rad);
        *this = this->translate(center);
        return *this;
    }

    Mat3x3<T>& translate(const Vec2<T> &vec)
    {
        //(*this) = (*this)*Vec3<T>(vec.x, vec.y, 1);
        *this = Mat3x3<T>::translation(vec)*(*this);
        return *this;
    }
    /*void rotate(angle, const Vec2<T> &center)
    {
        auto nt = Mat3x3<T>::translation(-c);
        auto r = Mat3x3<T>::rotation(rad);
        auto t = Mat3x3<T>::translation(c);
    }*/
    void transformation(const Mat2x2<T> &rot_scale, const Vec2<T> pos)
    {
        this->a11 = rot_scale.a11;
        this->a12 = rot_scale.a12;
        this->a21 = rot_scale.a21;
        this->a22 = rot_scale.a22;
        this->a13 = pos.x;
        this->a23 = pos.y;
        this->a33 = 1;
    }
    
    std::string to_string()
    {
        return "[" + this->row1().to_string() + "]\n[" + this->row2().to_string() + "]\n[" + this->row3().to_string() +"]";
    }
    
};
using Mat3f = Mat3x3<float>;

template<class T>
Vec2<T> operator*(const Vec2<T> &v, const Mat3x3<T> &m)
{
    Vec3<T> v3 = Vec3<T>(v.x, v.y, (T)1);
    v3 = v3 * m;
    Vec2<T> v2 = Vec2<T>(v3.x / v3.z, v3.y / v3.z);
    return v2;
}

template<class T>
Vec2<T> operator*(const Mat3x3<T> &m, const Vec2<T> &v)
{
    Vec3<T> v3 = Vec3<T>(v.x, v.y, (T)1);
    v3 = m * v3;
    Vec2<T> v2 = Vec2<T>(v3.x / v3.z, v3.y / v3.z);
    return v2;
}

template <class T>
Vec3<T> operator*(const Mat3x3<T> &m, const Vec3<T> &v)
{
    return Vec3<T>(dot(m.row1(), v), dot(m.row2(), v), dot(m.row3(), v));
}

template<class T>
Vec3<T> operator*(const Vec3<T> &v, const Mat3x3<T> &m)
{
    return Vec3<T>(dot(v, m.col1()), dot(v, m.col2()), dot(v, m.col3()));
}

template <class T>
bool operator==(const Mat3x3<T> &m1, const Mat3x3<T> &m2)
{

    return  m1.a11 == m2.a11 && m1.a12 == m2.a12 && m1.a13 == m2.a13 &&
            m1.a21 == m2.a21 && m1.a22 == m2.a22 && m1.a23 == m2.a23 &&
            m1.a31 == m2.a31 && m1.a32 == m2.a32 && m1.a33 == m2.a33;
}

template <class T>
Mat3x3<T> operator*(Mat3x3<T> m1, Mat3x3<T> m2)
{
/*    T v11 = m1.a11 * m2.a11 + m1.a12*m2.a21 + m1.a13*m2.a31; 
    T v12 = m1.a11 * m2.a12 + m1.a12*m2.a22 + m1.a13*m2.a32;
    T v13 = m1.a11 * m2.a13 + m1.a12*m2.a23 + m1.a13*m2.a33;

    T v21 = m1.a21 * m1.a11 + m1.a22*m2.a21 + m1.a23*m2.a31;
    T v22 = m1.a21 * m2.a12 + m1.a22*m2.a22 + m1.a23*m2.a32;
    T v23 = m1.a21 * m2.a13 + m1.a22*m2.a23 + m1.a23*m2.a33;
  
    T v31 = m1.a31 * m2.a11 + m1.a32*m2.a21 + m1.a33*m2.a31;
    T v32 = m1.a31 * m2.a12 + m1.a32*m2.a22 + m1.a33*m2.a32;
    T v33 = m1.a31 * m2.a13 + m1.a32*m2.a23 + m1.a33*m2.a33;
   
    return Mat3x3()*/

    return Mat3x3<T>(
        dot(m1.row1(), m2.col1()), dot(m1.row1(), m2.col2()), dot(m1.row1(), m2.col3()),
        dot(m1.row2(), m2.col1()), dot(m1.row2(), m2.col2()), dot(m1.row2(), m2.col3()),
        dot(m1.row3(), m2.col1()), dot(m1.row3(), m2.col2()), dot(m1.row3(), m2.col3())
        );


}


}
