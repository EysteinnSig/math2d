#pragma once

#include "vec2.h"

namespace Math2D {
    
template<class T>
struct Mat2x2 {
    T data[4];
    //T &a11=data[0], a12=0, a21=0, a22=0;
    T &a11=data[0], &a12=data[1], &a21=data[2], &a22=data[3];
    
    
    
    static Mat2x2<T> rotation(float angle)
    {
        return Mat2x2<T>(std::cos(angle), std::sin(angle), -std::sin(angle), std::cos(angle));
    }
    static Mat2x2<T> scaling(Vec2<T> scale)
    {
        return Mat2x2<T>(scale.x, 0, 0, scale.y);
    }

    static Mat2x2<T> identity() { return Mat2x2<T>(1, 0, 0,1); }

    inline Vec2<T> row1() { return Vec2<T>(a11, a12); }
    inline Vec2<T> row2() { return Vec2<T>(a21, a22); }
    inline Vec2<T> col1() { return Vec2<T>(a11, a21); }
    inline Vec2<T> col2() { return Vec2<T>(a12, a22); }

    Mat2x2<T>() 
    {}
    Mat2x2<T>(T a11_, T a12_, T a21_, T a22_) // : a11(a11_), a12(a12_), a21(a21_), a22(a22_)
    {
        a11=a11_;
        a12=a12_;
        a21=a21_;
        a22=a22_;
    }

};

template<class T> 
Mat2x2<T> operator*(Mat2x2<T> m1, Mat2x2<T> m2) 
{ 
    return Mat2x2<T>(dot(m1.row1(), m2.col1()), dot(m1.row1(), m2.col2()), dot(m1.row2(), m2.col1()), dot(m1.row2(), m2.col2()) );
}

template<class T> 
Vec2<T> operator*(Vec2<T> v, Mat2x2<T> m) 
{
    return Vec2<T>(dot(v, m.col1()), dot(v, m.col2()));
}

template<class T> 
Vec2<T> operator*(Mat2x2<T> m, Vec2<T> v) 
{
    return Vec2<T>(dot(v, m.row1()), dot(v, m.row2()));
}

/*template<class T>
Vec2<T> transform(Vec2<T> in, Mat2x2<T> m, Vec2<T> p)
{
    return (p-in)*m;
}*/

using Mat2f = Mat2x2<float>;


}
