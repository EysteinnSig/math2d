#pragma once

namespace Math2D {

template<class T>
struct __attribute__ ((__packed__)) Vec3 {

    union {
    struct {
        T x;
        T y;
        T z;
    };
    T v[3];
    };
    Vec3(T x, T y, T z) : x(x), y(y), z(z)
    {
        /*this->x = x;
        this->y = y;*/
    }
    Vec3(T v) : Vec3(v, v, v)
    {

    }
    Vec3() : Vec3(0)// : x(0), y(0) {}
    {
        /*x = 0;
        y = 0;*/
    }

    std::string to_string()
    {
        return "(" + std::to_string(x)+", "+std::to_string(y) + ", " + std::to_string(z) + ")";
    }
};

template<class T>
T dot(const Vec3<T> &a, const Vec3<T> &b) { return a.x*b.x+a.y*b.y+a.z*b.z; }

template <class T>
bool operator==(const Vec3<T> &v1, const Vec3<T> &v2) { return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z; }

using Vec3f = Vec3<float>;

}
