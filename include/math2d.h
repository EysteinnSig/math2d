#pragma once

#if !defined(PI)
    #define PI 3.14159265359
#endif

#include "mat2.h"
#include "mat3.h"
#include "vec2.h"
#include "vec3.h"


namespace Math2D {
    const double pi = 3.14159265359;
}
