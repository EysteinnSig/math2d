#pragma once

#include <cmath>
#include <string>
#include <algorithm>


namespace Math2D {

template<class T>
struct __attribute__ ((__packed__)) Vec2 {
//struct Vec2 {
    /*T data[2];
    T &x = data[0];
    T &y = data[1];*/
    //T x, y;

    union {
    struct {
        T x;
        T y;
    };
    T v[2];
    };
    Vec2(T x, T y) : x(x), y(y)//x(data[0]), y(data[1])// : x(x), y(y) {}
    {
        /*this->x = x;
        this->y = y;*/
    }
    Vec2() : Vec2(0)// : x(0), y(0) {}
    {
        /*x = 0;
        y = 0;*/
    }
    Vec2(T n) : Vec2(n,n) // : x(n), y(n) {}
    {
        /*this->x = n;
        this->y = n;*/
    }

    std::string to_string()
    {
        return "(" + std::to_string(x)+", "+std::to_string(y) + ")";
    }

    void operator=(const Vec2<T> &M ) { 
        this->x = M.x;
        this->y = M.y;
    }

    inline bool is_zero() const { return this->x == 0 && this->y == 0; }

    /*template<typename OStream>
    friend OStream &operator<<(OStream &os, const Vec2<T> &vec)
    {
        return os << "Vec2(" << vec.x << ", " << vec.y << ")";
    }*/

    Vec2<T> operator*(const Vec2<T>& b) const { return Vec2<T>(this->x*b.x, this->y*b.y); }
    Vec2<T> operator*(T b) { return Vec2<T>(this->x*b, this->y*b); }
    Vec2<T> operator/(T b) const { return (*this)*(1.0/b); }

    Vec2<T> operator-(const Vec2<T>& b) const { return Vec2<T>(this->x-b.x, this->y-b.y); }
    Vec2<T> operator+(const Vec2<T>& b) { return Vec2<T>(this->x+b.x, this->y+b.y); }

    //operator Vec2<int>() const { return Vec2<int>(std::round(this->x), std::round(this->y)); }

    template<class Archive>
    void save(Archive & archive) const
    {
        T x = this->x;
        T y = this->y;
        archive( x, y ); 
    }

    template<class Archive>
    void load(Archive & archive)
    {
        T x;
        T y;
        archive( x, y );
        this->x = x;
        this->y = y; 
    }

    /*template<class Archive>
    void serialize(Archive & archive)
    {
        archive( this->x, this->y );
    }*/

    float length()
    {
        return sqrt(this->x*this->x + this->y*this->y);
    }
    float length_squared()
    {
        return this->x*this->x + this->y*this->y;
    }

    static Vec2<T> lerp(const Vec2<T> &from, const Vec2<T> &to, float t)
    {

        t = std::clamp(t, 0.0f, 1.0f);
        return from + (to - from)*t;
    }
#ifdef MATH_USE_LUA

    static void register_lua_vec(sol::state &lua)
    {
        lua.new_usertype<Vec2<T>>("vec2",
            sol::constructors< Vec2<T>(), Vec2<T>(T), Vec2<T>(T, T) >(),
            "x", &Vec2<T>::x,
            "y", &Vec2<T>::y,
            //"__call", []() { printf("ABC\n"); },
            //sol::meta_function::call, [](T x, T y) { return Vec2<T>(x, y); },
            //https://github.com/ThePhD/sol2/blob/develop/examples/source/usertype_special_functions.cpp
            //sol::meta_function::addition, sol::resolve<Vec2<T>(const Vec2<T>&, const Vec2<T>&)>(::operator+)
            sol::meta_function::addition, &Vec2<T>::operator+,
            sol::meta_function::subtraction, &Vec2<T>::operator-
        );
    }
#endif
};


template <class T>
Vec2<T> operator+(const Vec2<T>& a, const Vec2<T>& b)  { return Vec2<T>(a.x+b.x, a.y+b.y); }

template <class T>
bool operator!=(const Vec2<T>& lhs, const Vec2<T>& rhs) { return !(lhs == rhs); }

template <class T, class D>
Vec2<T> operator*(const D a, const Vec2<T>& b)  { return Vec2<T>(a*b.x, a*b.y); }



/*
template<class T> 
Vec2<T> operator*(Vec2<T> a, Vec2<T> b) { return Vec2<T>(a.x*b.x, a.y*b.y); }

template<class T, class D> 
Vec2<T> operator*(Vec2<T> a, D b) { return Vec2<T>(a.x*b, a.y*b); }
template<class T, class D> 
Vec2<T> operator*(D a, Vec2<T> b) { return Vec2<T>(b.x*a, b.y*a); }

template<class T, class D> 
Vec2<T> operator+(Vec2<T> a, D b) { return a+b; }//return Vec2<T>(a.x+b, a.y+b); }
template<class T, class D> 
Vec2<T> operator+(D a, Vec2<T> b) { return Vec2<T>(b.x+a, b.y+a); }

template<class T, class D> 
Vec2<T> operator/(Vec2<T> a, D b) { return a*(1.0/b); }

template<class T> 
Vec2<T> operator+(Vec2<T> a, Vec2<T> b) { return Vec2<T>(a.x+b.x, a.y+b.y); }

template<class T> 
Vec2<T> operator-(Vec2<T> a, Vec2<T> b) { return Vec2<T>(a.x-b.x, a.y-b.y); }

template<class T, class D> 
Vec2<T> operator-(Vec2<T> a, D b) { return Vec2<T>(a.x-b, a.y-b); }
template<class T, class D> 
Vec2<T> operator-(D a, Vec2<T> b) { return Vec2<T>(a-b.x, a-b.y); }
template<class T> 
Vec2<T> operator-(Vec2<T> a) { return Vec2<T>(-a.x, -a.y); }

*/


template<class T>
inline bool operator==(const Vec2<T>& lhs, const Vec2<T>& rhs){ return lhs.x == rhs.x && lhs.y == rhs.y; }

template<class T>
inline Vec2<T> operator-(const Vec2<T>& vec) { return Vec2<T>(-vec.x, vec.y); }


template<class T>
T dot(Vec2<T> a, Vec2<T> b) { return a.x*b.x+a.y*b.y; }



using Vec2f = Vec2<float>;
using Vec2i = Vec2<int>;

inline Vec2i to_vec2i(const Vec2f &vec) { return Vec2i(std::round(vec.x), std::round(vec.y)); }
inline Vec2f to_vec2f(const Vec2i &vec) { return Vec2f(vec.x, vec.y); }

}




namespace std
{
    template<class T> 
    struct hash<Math2D::Vec2<T>>
    {
        std::size_t operator()(Math2D::Vec2<T> const& vec2) const noexcept
        {
            return std::hash<T>()(vec2.x) ^ std::hash<T>()(vec2.y);
        }
    };

    template<class T> 
    Math2D::Vec2<T> round(const Math2D::Vec2<T> &v)
    {
        return Math2D::Vec2<T>(std::round(v.x), std::round(v.y));
    }
    
}
