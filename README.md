# math2d

Basic 2d math library, Vectors and Matrices

## Description
This is a small and simple 2d math library to support my other experiments with opengl graphics.

## Includes:

- Templated math library for simple 2d graphics projects.
- Headers only.
- Supports 2d and 3d vectors and matrices.

## Build instructions:

For Linux: 
```
mkdir build && cd build && cmake ../ && make
```

## Order of operations:

This library follows the standard OpenGL operation order:

TransformedVector = TranslationMatrix * RotationMatrix * ScaleMatrix * OriginalVector;

The order of operation is from right to left, first scaling, then rotation, then translation.

## Examples:

A 2d vector of (1,1) is scaled by factor 2, rotated by pi/2 and translated along x axis by 2:
```
auto transformation = Mat3f::translation(Vec2f(2,0))*Mat3f::rotation(0.5*pi)*Mat3f::scaling(Vec2f(2,2));
std::cout << (transformation*Vec2f(1, 1)).to_string() << std::endl;
```
Returns: (0.000000, 2.000000)

This can be written in a sligtly different way giving the same result, notice the order of operation is now from left to right:
```
auto transformation = Mat3f::identity().scale(Vec2f(2)).rotate(0.5*pi).translate(Vec2f(2,0));
```

