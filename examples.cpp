#include "include/math2d.h"
#include <cstdio>
#include <iostream>


bool equals(float v1, float v2, float delta = 0.01)
{
    return std::abs(v1-v2) <= delta;
}

int main()
{
    using namespace Math2D;

    {
        auto transformation = Mat3f::translation(Vec2f(2,0))*Mat3f::rotation(0.5*pi)*Mat3f::scaling(Vec2f(2,2));
        std::cout << (transformation*Vec2f(1, 1)).to_string() << std::endl;
    }

    {
        auto transformation = Mat3f::identity().scale(Vec2f(2)).rotate(0.5*pi).translate(Vec2f(2,0));
        std::cout << (transformation*Vec2f(1, 1)).to_string() << std::endl;
    }
    
    return 0;   
}